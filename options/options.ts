import {
    AppSiteGetPluginConfigRetrievedResult,
    ReceiveOnceChannels,
    SendChannels,
} from "./api";
import { Dictionary } from "../types";
import { Window } from "./app";
import { debugLog, getMultiLanguage, getPluginAndSite } from "./helpers";
import { Tippy4PubliiConfig } from "../config";

declare let parent: Window;

const timeout = 1 * 60 * 1000;

/**
 * Tries to load our custom settings for the custom settings view.
 *
 * Ref. `getPluginConfig` in publii's `app/back-end/plugins.js`.
 * Loads file from `path.join(this.sitesDir, siteName, 'input', 'config', 'plugins', pluginName + '.json')`.
 * @param {*} siteName
 * @param {*} pluginName
 * @return {Promise<AppSiteGetPluginConfigRetrievedResult>} our plugin config and data or nothing
 */
async function loadCustomSettings(
    siteName,
    pluginName,
): Promise<AppSiteGetPluginConfigRetrievedResult> {
    // I do not remember why I wrote this, but it's working?
    const result: Promise<AppSiteGetPluginConfigRetrievedResult> = new Promise(
        (resolve, reject) => {
            setTimeout(
                reject,
                timeout,
                "Loading tippy4publii config took too long.",
            );
            // Handle response as promise
            globalThis.configReceived = {
                set: (value) => {
                    if (value === false) {
                        reject("Could not load tippy4publii config.");
                    } else resolve(value);
                },
            };
            // Send request to get plugin config
            parent.mainProcessAPI.send(SendChannels.AppSiteGetPluginConfig, {
                siteName,
                pluginName,
            });
            // Receive result of previous request
            parent.mainProcessAPI.receiveOnce(
                ReceiveOnceChannels.AppSiteGetPluginConfigRetrieved,
                (result) => {
                    debugLog(result, parent.mainProcessAPI.getEnv());
                    if (!result) {
                        globalThis.configReceived.set(false);
                    } else {
                        globalThis.configReceived.set(result);
                    }
                },
            );
        },
    );

    return result;
}

/**
 *  As the pendant to `loadCustomSettings`, this saves our custom settings as a file.
 *
 * Ref. `savePluginConfig` in publii's `app/back-end/plugins.js`.
 * Saves file at `path.join(this.sitesDir, siteName, 'input', 'config', 'plugins', pluginName + '.json')`.
 * @param {string} siteName
 * @param {string} pluginName
 * @param {Dictionary<any>} newConfig
 * @return {Promise<true>}
 */
async function saveCustomSettings(
    siteName: string,
    pluginName: string,
    newConfig: Dictionary<any>,
): Promise<true> {
    const result: Promise<true> = new Promise((resolve, reject) => {
        setTimeout(
            reject,
            timeout,
            "Saving tippy4publii config took too long.",
        );
        // Handle response as promise
        globalThis.configSaved = {
            set: (value) => {
                if (!value) {
                    reject("Could not save tippy4publii config.");
                } else resolve(value);
            },
        };
        // Send request to save plugin config
        parent.mainProcessAPI.send(SendChannels.AppSiteSavePluginConfig, {
            siteName: siteName,
            pluginName: pluginName,
            newConfig: newConfig,
        });
        // Receive result of previous request
        parent.mainProcessAPI.receiveOnce(
            ReceiveOnceChannels.AppSitePluginConfigSaved,
            async (result) => {
                if (result === true) {
                    globalThis.configSaved.set(true);
                } else {
                    globalThis.configSaved.set(false);
                }
            },
        );
    });
    return result;
}

/**
 * Translate plugin options into target language specified in config.
 *
 * @param {string} [target="en"]  Target language for translation
 */
async function translate(target: string = "en") {
    // Load translator for target language
    const ml = getMultiLanguage(parent.mainProcessAPI, target);
    // Collect keys to translate
    const toTranslate = document.querySelectorAll(
        "*[data-ml-text]",
    ) as NodeListOf<HTMLElement>;
    // Log strings to translate if dev
    debugLog(toTranslate, parent.mainProcessAPI.getEnv());
    // Translate them all
    toTranslate.forEach(async (element: HTMLElement) => {
        const key = element.dataset.mlText ?? "";
        try {
            // If translation was found, replace the text
            element.innerHTML = (await ml.translate(key)) ?? element.innerHTML;

            debugLog(element.innerHTML, parent.mainProcessAPI.getEnv());
        } catch (error) {
            // Log our error if dev
            const env = parent.mainProcessAPI.getEnv();
            debugLog(
                `During the translation of "${element.innerHTML}" an error occured.`,
                env,
            );
            debugLog(error, env);
        }
    });
}

/**
 * WIP
 * Prepares the settings page.
 * This includes loading the settings
 * and translating the page into the target language.
 *
 */
async function preLoad() {
    // prepare the config
    const [siteName, pluginName] = getPluginAndSite();
    const settings = await loadCustomSettings(siteName, pluginName);
    let config = new Tippy4PubliiConfig();
    if (settings.pluginConfig) {
        // TODO: instead merge dicts
        config = JSON.parse(settings.pluginConfig) as Tippy4PubliiConfig;
    }
    debugLog(config, parent.mainProcessAPI.getEnv());
    // get data ml text and translate
    translate(config.language);
    // TODO: fill html with loaded config options
}
/**
 * WIP, submit the config
 *
 * @export
 */
export async function submit() {
    const [siteName, pluginName] = getPluginAndSite();
    // TODO: get real config
    saveCustomSettings(siteName, pluginName, { aa: "naa" });
    // while saving, display loading bar/circle/... don't allow/disable input
    // if error, display error
    // else display success
    // ... refetch config, ...
    // ... and enable/allow input
    debugLog("Submit", parent.mainProcessAPI.getEnv());
}

// maybe save to custom config file?
preLoad().then(() => {
    const submitButton = document.querySelector("#wassupMIT");
    if (!submitButton) {
        debugLog("Missing submit button!", parent.mainProcessAPI.getEnv());
    }
    // Ensure the submit button does actual submitting
    submitButton?.addEventListener("click", submit);
});
