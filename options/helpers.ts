/**
 * What's a project without a messy file named helpers?
 */

import { Window } from "./app";
import { MainProcessAPI, ProcessEnv } from "./api";
import { Filesystem, MultiLanguage } from "../multi_language";
/**
 * If we are running in development, this function logs whatever it is passed.
 *
 * @export
 * @param {*} message
 * @param {ProcessEnv} env
 */
export function debugLog(message: any, env: ProcessEnv) {
    if (env.name == "development") {
        console.log("Tippy4Publii:");
        console.log(message);
    }
}

declare let parent: Window;

interface CurrentEntry {
    url: string;
}

interface Navigation {
    currentEntry: CurrentEntry;
}

declare let navigation: Navigation;

/**
 * To be used on plugin config page.  Site name should work everywhere, but the plugin name is extracted from the route.
 *
 * @export
 * @return {string[]} site name and plugin name
 */
export function getPluginAndSite(): string[] {
    const siteName = parent.app.getSiteName();
    const url = navigation.currentEntry.url;
    const route = url.split("#")[0];
    const pluginName = route.split("plugins/")[1].split("/")[0];
    debugLog(pluginName, parent.mainProcessAPI.getEnv());
    return [siteName, pluginName];
}
/**
 * Creates a Tippy4Publii MultiLanguage object to trangslate the config with.
 *
 * @export
 * @param {MainProcessAPI} mainProcessAPI
 * @param {string} language
 * @return {MultiLanguage} our translator
 */
export function getMultiLanguage(
    mainProcessAPI: MainProcessAPI,
    language: string,
): MultiLanguage {
    // Cobble together a filesystem using the main process API and jquery, yeehaw!
    const fs: Filesystem = {
        path: (...paths: string[]) => {
            const path: string = paths.join("/");
            return mainProcessAPI.normalizePath(path);
        },
        exists: (path: string) => {
            return mainProcessAPI.existsSync(path);
        },
        readJson: async (path: string) => {
            const raw = await parent.$.getJSON(path);
            return raw;
        },
    };

    const sitePath = parent.app.getSiteDir();
    const [, pluginName] = getPluginAndSite();

    return new MultiLanguage(sitePath, pluginName, language, fs, (message) => {
        debugLog(message, mainProcessAPI.getEnv());
    });
}
