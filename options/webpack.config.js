const path = require("path");

module.exports = {
    entry: "./options.ts",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
            },
        ],
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
    },
    mode: "production",
    output: {
        filename: "options.js",
        path: path.resolve(__dirname, "../plugin/options"),
    },
};
