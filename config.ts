/**
 * Nothing much to see here... yet.
 */

export class Tippy4PubliiConfigCustom {
    useTippyShortcodes: boolean = true;
}

export class Tippy4PubliiConfig extends Tippy4PubliiConfigCustom {
    useUnpkg: boolean = true;
    language: string = "en";
}
