"use strict";
/**
 * Types, interfaces, and classes for publii.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PubliiPlugin = void 0;
/* eslint-disable @typescript-eslint/ban-types */ // TODO: remove "function" type
class PubliiPlugin {
    constructor(api, name, config) {
        this.api = api;
        this.name = name;
        this.config = config;
    }
}
exports.PubliiPlugin = PubliiPlugin;
