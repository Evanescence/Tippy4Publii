"use strict";
/** Exports instance of this plugin. */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const types_1 = require("./types");
const path_1 = __importDefault(require("path"));
const fs_1 = require("fs");
const multi_language_1 = require("./multi_language");
const config_1 = require("./config");
class Tippy4Publii extends types_1.PubliiPlugin {
    /**
     * Creates an instance of Tippy4Publii.
     * @param {RendererPlugins} api renderer plugins api passed by publii
     * @param {string} name the plugin name used in publii folder structure
     * @param {Dictionary<any>} config config read from plugin.json
     * @memberof Tippy4Publii
     */
    constructor(api, name, config) {
        super(api, name, config);
        // load standard config values
        this.config = new config_1.Tippy4PubliiConfig();
        // override standard config values with local
        Object.entries(config).map(([option, value]) => {
            if (this.config[option] && option !== "language")
                this.config[option] = value;
        });
        // load language file
        const nodeFs = {
            path: (...paths) => Promise.resolve(path_1.default.join(...paths)),
            exists: (path) => Promise.resolve((0, fs_1.existsSync)(path)),
            readJson: (path) => {
                const fileContent = (0, fs_1.readFileSync)(path).toString();
                return Promise.resolve(JSON.parse(fileContent));
            },
        };
        if (config["language"] && typeof config["language"] === "string") {
            this.translation = new multi_language_1.MultiLanguage(api.sitePath, name, config["language"], nodeFs, () => {
                return {};
            });
        }
        else {
            this.translation = new multi_language_1.MultiLanguage(api.sitePath, name, "en", nodeFs, (e) => {
                throw e;
            });
        }
    }
    /**
     * Infers the asset url and use it to load bundled scripts.
     * Only needed if useUnpkg is false.
     *
     * @param {string} domain site domain to use
     * @return {string} asset url without trailing slash, e.g. test.com/media/plugins/tippy4publii
     * @memberof Tippy4Publii
     */
    _getAssetUrl(domain) {
        return domain + "/media/plugins/" + this.name;
    }
    /**
     * Constructs script tags using the asset url and script file's name.
     *
     * @param {string} url asset url
     * @param {string} script script file name
     * @return {string} html script tag
     * @memberof Tippy4Publii
     */
    _getScriptTag(url, script) {
        return `<script src="${url}/${script}"></script>\n`;
    }
    /**
     * Constructs style link using the asset url and style's file name.
     *
     * @param {string} url asset url
     * @param {string} style style file name
     * @return {string} link rel stylesheet tag
     * @memberof Tippy4Publii
     */
    _getStyleTag(url, style) {
        return `<link rel="stylesheet" href="${url}/${style}"></style>\n`;
    }
    /**
     * Ensure scripts and styles are inserted into the head during rendering.
     *
     * @memberof Tippy4Publii
     */
    addInsertions() {
        this.api.addInsertion("publiiHead", this.insertTippyHead, 1, this);
    }
    /**
     * Inserts scripts and styles for using tippy.
     * If useUnpkg is false, bundled scripts will be used.
     *
     * @param {Renderer} rendererInstance
     * @return {string} scripts and styles necessary to use tippy
     * @memberof Tippy4Publii
     */
    insertTippyHead(rendererInstance) {
        let insert = this._getScriptTag("https://unpkg.com/@popperjs/core@2/dist/umd", "popper.min.js") +
            this._getScriptTag("https://unpkg.com/tippy.js@6/dist", "tippy-bundle.umd.js");
        if (!this.config.useUnpkg) {
            const url = this._getAssetUrl(rendererInstance.siteConfig.domain);
            insert =
                this._getScriptTag(url, "popper.min.js") +
                    this._getScriptTag(url, "tippy.umd.min.js") +
                    this._getStyleTag(url, "tippy.css");
        }
        return insert;
    }
}
module.exports = Tippy4Publii;
