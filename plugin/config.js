"use strict";
/**
 * Nothing much to see here... yet.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tippy4PubliiConfig = exports.Tippy4PubliiConfigCustom = void 0;
class Tippy4PubliiConfigCustom {
    constructor() {
        this.useTippyShortcodes = true;
    }
}
exports.Tippy4PubliiConfigCustom = Tippy4PubliiConfigCustom;
class Tippy4PubliiConfig extends Tippy4PubliiConfigCustom {
    constructor() {
        super(...arguments);
        this.useUnpkg = true;
        this.language = "en";
    }
}
exports.Tippy4PubliiConfig = Tippy4PubliiConfig;
