"use strict";
/** Handle translations. */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MultiLanguage = void 0;
/**
 * Enables bundling translations with the plugin.
 * Use translate function to translate a key.
 * Passes error messages to callback.
 *
 * @export
 * @class MultiLanguage
 */
class MultiLanguage {
    /**
     * Loads language file using our cobbled together fs.
     * If the file is not found, English is used.
     *
     * @static
     * @param {string} sitePath
     * @param {string} pluginName
     * @param {string} [language="en"] language to use, standard is en
     * @param {Filesystem} fs cobbled together filesystem
     * @param {LanguageErrorCallback} onError callback in case of errors
     * @return {Promise<Dictionary<any>>} mapping of key to translation or empty mapping if error
     * @memberof MultiLanguage
     */
    static getLanguageFile(sitePath, pluginName, language = "en", fs, onError) {
        return __awaiter(this, void 0, void 0, function* () {
            const languagesPath = yield fs.path(sitePath, "../../", "plugins", pluginName, "languages");
            // use English
            let useFile = yield fs.path(languagesPath, "en.json");
            // unless...
            if (language !== "en") {
                const languageFile = yield fs.path(languagesPath, `${language}.json`);
                if (yield fs.exists(languageFile)) {
                    useFile = languageFile;
                }
                else {
                    onError("Couldn't find language file. Using English instead...");
                }
            }
            try {
                return yield fs.readJson(useFile);
            }
            catch (_a) {
                onError("Language file has invalid JSON structure or is unreadable.");
                return {};
            }
        });
    }
    constructor(sitePath, pluginName, language, fs, onError) {
        this.sitePath = sitePath;
        this.pluginName = pluginName;
        this.language = language;
        this.fs = fs;
        this.onError = onError;
    }
    /**
     * Return translation for key
     * or null if no matching translation was found.
     *
     * @param {string} key
     * @return {Promise<string>}
     * @memberof MultiLanguage
     */
    translate(key) {
        return __awaiter(this, void 0, void 0, function* () {
            // Load all translations for configured language
            const translations = yield MultiLanguage.getLanguageFile(this.sitePath, this.pluginName, this.language, this.fs, this.onError);
            if (key in translations) {
                // Ensure that we return a string
                const translation = translations[key];
                return typeof translation === "string" ? translation : null;
            }
            return null;
        });
    }
}
exports.MultiLanguage = MultiLanguage;
