Todo Collection
==================

_uncertain location / multi location_:
----------------------------------------
- ensure other config is translated as well
- better error handling in production
- add multiple debug levels to reduce spam
- ... or add a config option to disable debug log

main.ts
-----------------
`_getAssetUrl`:
- add option to change the asset url in config/settings?
- ensure above does mess with preview

`constructor`:
- load custom settings from file

multi_language.ts
------------------------
`translate`:
- speed this up
- if there ever are more than a few hundred keys (unlikely?), split up

types.ts
------------------
- ensure nothing is outdated // constant struggle eeh
- remove "function" and "any" types, so that we can remove the eslint comment

config.ts
---------------------
- ...

options/options.ts
----------------------
- use a separate config/different file?

`saveCustomSettings`:
- figure out why this returns promise of true

`preLoad`:
- instead merge dicts?
- fill html with loaded config options

`submit`:
- get real config
- while saving, display loading bar/circle/... don't allow/disable input
- if error, display error
- else display success, refetch config, and enable/allow input

options/api.ts
-------------------------
same as types.ts:
- ensure this is not out of date
- improve...

plugin/options/index.html
----------------------------
- keep copy in options/ and copy during transpiling of typescript?
- expand alongside custom settings aka config:
  - custom tippy scripts + style
  - custom shortcodes
  - tinymce mod?


How to transpile
===================
First, use eslint (npx eslint) and prettier (npx prettier or codium format document with), then
- cd options -> npx webpack
- cd .. -> npx tsc
