/** Handle translations. */

import { Dictionary } from "./types";

export type LanguageErrorCallback = (message: string) => void;

export interface Filesystem {
    path(...paths: string[]): Promise<string>;
    exists(path: string): Promise<boolean>;
    readJson(path: string): Promise<Dictionary<any>>;
}

/**
 * Enables bundling translations with the plugin.
 * Use translate function to translate a key.
 * Passes error messages to callback.
 *
 * @export
 * @class MultiLanguage
 */
export class MultiLanguage {
    sitePath: string;
    pluginName: string;
    language: string;
    fs: Filesystem;
    onError: LanguageErrorCallback;

    /**
     * Loads language file using our cobbled together fs.
     * If the file is not found, English is used.
     *
     * @static
     * @param {string} sitePath
     * @param {string} pluginName
     * @param {string} [language="en"] language to use, standard is en
     * @param {Filesystem} fs cobbled together filesystem
     * @param {LanguageErrorCallback} onError callback in case of errors
     * @return {Promise<Dictionary<any>>} mapping of key to translation or empty mapping if error
     * @memberof MultiLanguage
     */
    static async getLanguageFile(
        sitePath: string,
        pluginName: string,
        language: string = "en",
        fs: Filesystem,
        onError: LanguageErrorCallback,
    ): Promise<Dictionary<any>> {
        const languagesPath = await fs.path(
            sitePath,
            "../../",
            "plugins",
            pluginName,
            "languages",
        );
        // use English
        let useFile = await fs.path(languagesPath, "en.json");
        // unless...
        if (language !== "en") {
            const languageFile = await fs.path(
                languagesPath,
                `${language}.json`,
            );
            if (await fs.exists(languageFile)) {
                useFile = languageFile;
            } else {
                onError(
                    "Couldn't find language file. Using English instead...",
                );
            }
        }
        try {
            return await fs.readJson(useFile);
        } catch {
            onError(
                "Language file has invalid JSON structure or is unreadable.",
            );
            return {};
        }
    }

    constructor(
        sitePath: string,
        pluginName: string,
        language: string,
        fs: Filesystem,
        onError: LanguageErrorCallback,
    ) {
        this.sitePath = sitePath;
        this.pluginName = pluginName;
        this.language = language;
        this.fs = fs;
        this.onError = onError;
    }

    /**
     * Return translation for key
     * or null if no matching translation was found.
     *
     * @param {string} key
     * @return {Promise<string>}
     * @memberof MultiLanguage
     */
    async translate(key: string): Promise<string | null> {
        // Load all translations for configured language
        const translations = await MultiLanguage.getLanguageFile(
            this.sitePath,
            this.pluginName,
            this.language,
            this.fs,
            this.onError,
        );
        if (key in translations) {
            // Ensure that we return a string
            const translation = translations[key];
            return typeof translation === "string" ? translation : null;
        }
        return null;
    }
}
